package es.dgonzalobo;

import java.io.*;


public class PasswordPorConsola { 
	private String clave = null;

	public PasswordPorConsola () { 
		try{
			Console consola = null;
			consola = System.console(); // creates a console object
			if (consola != null) { // read password into the char array
				char[] pwd = consola.readPassword();
				clave=new String(pwd); 
			}
			else {
				borraConsolaThread borraconsola = new borraConsolaThread(); 
				BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in)); 
				borraconsola.start();
				clave = stdin.readLine();
				borraconsola.parar();
			}
		}
		catch(Exception ex){
			System.out.println ("PasswordPorConsola Exception: "+ex);
		}
	} //constructor PasswordPorConsola

	public String toString (){ 
		return clave;
	}
}


class borraConsolaThread extends Thread { 
	private boolean salir = false;

	public void run() {
		try {
			while (!salir) { 
				System.out.print(" ");
				sleep(10); 
			} //while
		} 
		catch(InterruptedException ex2){ } 
	}

	public synchronized void parar() { 
		salir = true;
	}
}
