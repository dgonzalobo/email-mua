/**
 * @author dgonzalobo
 * (Desarrollado en UNIX)
 * 
 * PRACTICA 10 (Opcional)
 * Implementar un programa para un mensaje de correo compuesto de un texto y un fichero adjunto (mensaje multiparte), 
 * usando una cuenta de uniovi (outlook) o gmail como servidor con el protocolo smtp en el puerto 587.
 * 
 * Usar URLName, leer password, subject destinatario, nombre del fichero adjunto y contenido del mensaje por teclado.
 * 
 * MEJORA 1
 * Se le añade un menu, que permite una mejor interaccion del usuario con el probrama, de tal modo, que este pueda en todo momento
 * seguir mandando email, cambiar el usuario o salir del programa.
 * 
 * MEJORA 2
 * Solicita al arrancar, el nombre que quiere que figure en la cabecera del email enviado.
 * 
 * MEJORA 3
 * Se da la opción, sin cerrar el MUA, de cambiar el usuario desde el que se manda, de tal manera que se vuelve a solicitar el 
 * hots del server que da servicio, el usuario desde el que se quiere mandar y el nombre de cabecera del email.
 * 
 * MEJORA 4 
 * Se da la opcion de añadir o no archivo adjunto.
 * 
 * MEJORA 5
 * Se da la opcion de mandar el email a varios destinatarios 
 * 
 */

package es.dgonzalobo;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class MUA {
	private static BufferedReader teclado;
	private static String host;
	private static String username;
	private Vector<String> cc;
	private String password, name;
	private static String protocolo;
	private static MUA cliente;
	private InternetAddress sendto;

	/**
	 * @funcion abre el flujo teclado, encargado durante la ejecución del programa de interactuar con el user por el teclado del
	 * 			dispositivo sobre el que sea ejecutado. Inicializa la variable password, name y protocolo a null.
	 */
	public MUA(){
		teclado = new BufferedReader(new InputStreamReader(System.in)); 
		password=null;
		name=null;
		protocolo=null;
		cc = new Vector<String>();
	}

	/**
	 * @funcion Si el programa es invocado sin parametros se le asignan por defecto, en caso contrario, son extraidos de la URL 
	 * 			a través de los métodos propio de la clase URLName. Posteriormente se muestra por pantalla una cabecera que ayuda
	 * 			al user a reconocer el programa. Se invoca al método name, y después al menu.
	 * @param args URL, recibe una URL con los datos necesarios para la ejecución del programa.
	 */
	public static void main(String[] args) {
		cliente=new MUA(); 

		if(args.length!=1){
			host= "smtp.gmail.com";
			username = "dgonzalobo@gmail.com"; 
			protocolo="smtp";
		}
		else{
			URLName url=new URLName(args[0]);
			host=url.getHost();
			username=url.getUsername();
			protocolo=url.getProtocol();
		}

		System.out.println("\t****************************************************");
		System.out.println("\t                         MUA");
		System.out.println("\t                    ("+username+")");
		System.out.println("\t****************************************************\n");
		cliente.name();
		cliente.menu();
	}

	/**
	 * @funcion inicialmente se crea una variable props de tipo Porperties, donde se almacenarán las propiedades del email a enviar.
	 * 			Posteriormente se crea una sesion y una variable MimeMessage, donde se almacenará todo lo necesario para el envio 
	 * 			del email. Tras añadir el mail del usuario que envío, el del receptor y el asunto, creamos un MimeMultipart para 
	 * 			alamacenar dentro del msg tanto el texto como el archivo adjunto. 
	 * 			Por ultimo se crea un conexion email a través de un objeto de la casle transport, se solicita la contraseña de la	
	 * 			cuenta email al usuario, apoyandonos en la clase auxiliar PasswordPorConsola, se connecta con el servidor, enviandole
	 * 			el username y la password a través del objeto t creado anteriormente, se envía el mensaje a dicho servidor si la 
	 * 			conexión se ha producido y se muestra por pantalla una mensaje notificando que el envío se ha producido correctamente.
	 */
	public void run(){

		try {
			Properties props = new Properties(); 
			props.put("mail.smtp.host", host); 
			props.put("mail.smtp.port", "587"); 
			props.put("mail.smtp.starttls.enable", "true"); 
			props.put("mail.smtp.auth", "true");

			Session mailConnection = Session.getInstance(props);
			MimeMessage msg = new MimeMessage(mailConnection);
			InternetAddress me = new InternetAddress(username,name); 
			to();

			msg.setFrom(me);
			msg.setRecipient(Message.RecipientType.TO,sendto);
			msg.setSubject(subject());

			MimeMultipart multiParte=new MimeMultipart();
			System.out.println("¿Desea añadir adjunto? (S/N)");
			if(teclado.readLine().equalsIgnoreCase("S"))
				multiParte.addBodyPart(adjunto());
			multiParte.addBodyPart(text()); 
			msg.setContent(multiParte);

			Transport t = mailConnection.getTransport(protocolo); 
			System.out.println("Introduzca el password para "+ username +": ");
			password= (new PasswordPorConsola()).toString(); 
			System.out.println("Password Leida.");

			t.connect(username, password);
			t.sendMessage(msg, msg.getAllRecipients());
			if(cc.size()>0){
				for(int i=0; i<cc.size();i++){
					sendto = new InternetAddress(cc.elementAt(i),"Yo"+i);
					msg.setRecipient(Message.RecipientType.TO,sendto);
					t.sendMessage(msg, msg.getAllRecipients());
				}
			}
			System.out.println("¡MENSAJE ENVIADO!");
		}
		catch(MessagingException e){
			System.out.println("ERROR || Alguno de los parametros estaba mal, imposible mandar email.");
			menu();
		}
		catch (Exception ex) {
			System.err.println("ERROR || Metodo Run, exception: "+ex); 
			System.exit(0);
		}
	}

	/**
	 * @funcion solicita por teclado el asunto del email.
	 * @return subject, un String que contiene la cadena con el asunto del email.
	 */
	public String subject(){
		String subject=null;
		try{
			System.out.println("-.Asunto: ");
			subject=teclado.readLine();
		}
		catch(IOException e){
			System.err.println("ERROR || Metodo subject, exception: "+e);
		}
		return subject;

	}

	/**
	 * @funcion solicita por teclado la direccion email a la que se va a mandar el mensaje. Una vez introducida se alamcenará
	 * 			en una variable internetAddress que posteriormente será necesaria para el correcto tratamiento de msg.setRecipient
	 * 			Ante la mejora que supone el ponder mandarlos a varios usuarios, se pregunta si se quiere enviar a más de un usuarios
	 * 			a cuantos y se piden sus direcciones de email, que serán almacenadas en un vector cc.
	 */
	public void to(){
		try{
			System.out.println("-.Para:");
			String aux=teclado.readLine();
			sendto = new InternetAddress(aux,"Yo"); 
			System.out.println("¿Desea enviarlo a más usuarios?(S/N)");
			if(teclado.readLine().equalsIgnoreCase("S")){
				System.out.println("¿A cuantos desa enviar?");
				int n=Integer.parseInt(teclado.readLine());
				for(int i=0;i<n;i++){
					System.out.println("-.CC:");
					cc.add(teclado.readLine());
				}
			}
		}
		catch(Exception e){
			System.err.println("ERROR || Metodo to, exception: "+e);
		}
	}

	/**
	 * @funcion Solicita por teclado el nombre del archivo adjunto. Posteriormente a traves de setDataHandler y setFileName, se 
	 * 			procede a introducir dicho archivo en una variable MimeBodyPart, que conformará junto con el mensaje del email,
	 * 			el mensaje multipart que será enviado.
	 * @return adjunto, mimeBodyPart que contiene el archivo a adjuntar.
	 */
	public BodyPart adjunto(){
		BodyPart adjunto = new MimeBodyPart(); 

		try{
			System.out.println("-.Adjunto: ");
			String aux=teclado.readLine();
			adjunto.setDataHandler(new DataHandler(new FileDataSource(aux))); 
			adjunto.setFileName(aux);
		}
		catch(Exception e){
			System.err.println("ERROR || Metodo adjunto, exception: "+e);
		}

		return adjunto;
	}

	/**
	 * @funcion Se solicita por teclado el texto a enviar en email, hasta que el usuario introduzca punto en una sola linea. 
	 * 			Una vez alamacenado el texto en el string text, se procede a introducir este en un mimeBodyPart.
	 * @return text, mimeBodyPart donde se almacena el texto a enviar en el email.
	 */
	public BodyPart text(){
		BodyPart texto = new MimeBodyPart(); 
		try{
			String text="";
			System.out.println("-.Texto (escribir '.' para parar): ");
			String aux=teclado.readLine();
			while(!aux.equals(".")){
				text=text+aux;
				aux=teclado.readLine();
			}
			texto.setText(text);
		}
		catch(Exception e){
			System.err.println("ERROR || Método text, exception: "+e);
		}
		return texto;
	}

	/**
	 * @funcion Se solicita por teclado el nuevo email desde el que se quiere enviar mensajes. Acto seguido, solicitamos el host
	 * 			del servidor del proveedor.
	 */
	public void changeUser(){
		try{
			System.out.println("Nuevo Usuario: ");
			username=teclado.readLine();
			System.out.println("Host del servidor de mail: ");
			host=teclado.readLine();
			name();
		}
		catch(IOException e){
			System.err.println("ERROR || Metodo changeUser, exception: "+e);
		}
		menu();
	}

	/**
	 * @funcion Menu que permite interactuar con el usuario.
	 */
	public void menu(){
		System.out.println("\n____________MENU____________");
		System.out.println("(1)...........Enviar Mensaje");
		System.out.println("(2)...........Cambiar Cuenta");
		System.out.println("(3)....................Salir");

		try{
			int accion=Integer.parseInt(teclado.readLine());
			System.out.println("\n");
			if(accion==1)
				run();
			if(accion==2)
				changeUser();
			if(accion==3)
				System.exit(0);
			else{
				menu();
			}		
		}
		catch(IOException e){
			System.err.println("ERROR || Metodo Menu, exception: "+e);
		}
	}

	/**
	 * @funcion Solicita por teclado el nombre que el usuario quiere que figure en la cabecera de su email.
	 */
	public void name(){
		try{
			System.out.println("Introduzca el su nombre: ");
			name=teclado.readLine();
		}
		catch(IOException e){
			System.err.println("ERROR || Metodo name, exception: "+e);
		}
	}
}

